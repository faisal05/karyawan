-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Bulan Mei 2022 pada 06.56
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyawan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_employee`
--

CREATE TABLE `m_employee` (
  `m_employee_id` int(8) NOT NULL,
  `nama_karyawan` varchar(250) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `hubungan_keluarga` varchar(100) NOT NULL,
  `nama_anggota_keluarga` varchar(250) NOT NULL,
  `tanggal_lahir_anggota_keluarga` date NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `create_by` int(8) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(8) NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_employee`
--

INSERT INTO `m_employee` (`m_employee_id`, `nama_karyawan`, `tanggal_lahir`, `alamat`, `email`, `hubungan_keluarga`, `nama_anggota_keluarga`, `tanggal_lahir_anggota_keluarga`, `valid_from`, `valid_to`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES
(22050002, 'Reza', '2022-05-20', 'sasasa', '', 'istri', 'sasasa', '2022-05-20', '2022-05-25', '2022-05-25', 22050002, '2022-05-25 04:46:42', 22050002, '2022-05-25 11:46:42'),
(22050003, 'Samsul', '1996-12-05', 'Jakarta', '', 'istri', 'Riri', '1997-11-20', '2022-05-25', '2022-05-25', 22050003, '2022-05-25 04:55:18', 22050003, '2022-05-25 11:55:18');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `m_employee`
--
ALTER TABLE `m_employee`
  ADD PRIMARY KEY (`m_employee_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `m_employee`
--
ALTER TABLE `m_employee`
  MODIFY `m_employee_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22050004;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
