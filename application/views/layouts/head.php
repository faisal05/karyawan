<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="description" content="" />
<meta name="author" content="" />

<title>Karyawan</title>

<!-- Custom fonts for this template -->
<link href="<?php echo base_url(); ?>assets/template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />

<!-- Custom styles for this template -->
<link href="<?php echo base_url(); ?>assets/template/css/sb-admin-2.min.css" rel="stylesheet" />

<!-- Jquery Confirm -->
<link href="<?php echo base_url(); ?>assets/jqueryconfirm/jquery-confirm.min.css" rel="stylesheet" />

<!-- Custom styles for this page -->
<link href="<?php echo base_url(); ?>assets/template/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" />