<!-- Bootstrap core JavaScript-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery/jquery-3.2.1.min.js"></script>


<script src="<?php echo base_url(); ?>assets/template/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- Core plugin JavaScript-->
<script src="<?php echo base_url(); ?>assets/template/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo base_url(); ?>assets/template/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="<?php echo base_url(); ?>assets/template/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="<?php echo base_url(); ?>assets/template/js/demo/datatables-demo.js"></script>

<!-- Jquery Confirm-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jqueryconfirm/jquery-confirm.min.js"></script>

<?php !empty($script) ? $this->load->view($script) : ""; ?>