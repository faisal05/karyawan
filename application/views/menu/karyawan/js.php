<script type="text/javascript">
    $(document).ready(function() {

        function BersihkanLayar() {
            location.reload(true);
        };

        function Bersihkanlayarbaru() {
            $('#kodekaryawan').val('');
            $('#namakaryawan').val('');
            $('#tgllahirkaryawan').val('');
            $('#alamatkaryawan').val('');
            $('#hubungankeluarga1').val('');
            $('#namakeluarga1').val('');
            $('#tgllahirkeluarga1').val('');
            $('#datakeluargamore').hide();
            refreshdata();
            document.getElementById('update').disabled = true;
            document.getElementById('remove').disabled = true;
        }

        Bersihkanlayarbaru();

        $("#adddatakeluarga").click(function() {
            var html = $("#datakeluargamore").html();
            $("#datakeluarga1").after(html);
        });

        $("#removedatakeluarga").click(function() {
            $("#datakeluarga1").remove();
        });

        function CekValidasi() {
            if ($('#namakaryawan').val() == '') {
                $.alert({
                    type: 'red',
                    title: 'Gagal!',
                    content: 'Nama Karyawan Belum Diisi',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red',
                            keys: ['enter', 'shift'],
                            action: function() {
                                $('#namakaryawan').focus();
                            }
                        }
                    }
                });
                var result = false;
            } else if ($('#tgllahirkaryawan').val() == '') {
                $.alert({
                    type: 'red',
                    title: 'Gagal!',
                    content: 'Tanggal lahir Belum Diisi',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red',
                            keys: ['enter', 'shift'],
                            action: function() {
                                $('#tgllahirkaryawan').focus();
                            }
                        }
                    }
                });
                var result = false;
            } else if ($('#alamatkaryawan').val() == '') {
                $.alert({
                    type: 'red',
                    title: 'Gagal!',
                    content: 'Alamat Karyawan Belum Diisi',
                    buttons: {
                        formSubmit: {
                            text: 'OK',
                            btnClass: 'btn-red',
                            keys: ['enter', 'shift'],
                            action: function() {
                                $('#alamatkaryawan').focus();
                            }
                        }
                    }
                });
                var result = false;
            } else {
                var result = true;
            }
            return result;
        };

        function refreshdata() {
            $('#datakaryawan').DataTable({
                "destroy": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "order": [],
                "ajax": {
                    "url": "<?php echo base_url('karyawan/caridata'); ?>",
                    "method": "POST",
                    "data": {
                        nmtb: "m_employee",
                        field: {
                            m_employee_id: "m_employee_id",
                            nama_karyawan: "nama_karyawan",
                            tanggal_lahir: "tanggal_lahir",
                            alamat: "alamat",
                            email: "email",
                            hubungan_keluarga: "hubungan_keluarga",
                            nama_anggota_keluarga: "nama_anggota_keluarga",
                            tanggal_lahir_anggota_keluarga: "tanggal_lahir_anggota_keluarga",
                            valid_from: "valid_from",
                            valid_to: "valid_to",
                            create_by: "create_by",
                            create_date: "create_date",
                            update_by: "update_by",
                            update_date: "update_date"
                        },
                        sort: "m_employee_id",
                        where: {
                            kodekaryawan: "m_employee_id"
                        },
                        value: ""
                    },
                }
            });
        }

        function cekdatadb() {
            var result = true;
            $.ajax({
                    url: "<?php echo base_url('karyawan/cekdata'); ?>",
                    method: "POST",
                    dataType: "json",
                    async: false,
                    data: {
                        kode: $('#kodekaryawan').val(),
                    },
                    success: function(data) {
                        if (data.length > 0) {
                            result = true;
                        } else {
                            result = false;
                        }
                    }
                },
                false);
            return result;
        }

        document.getElementById("add").addEventListener("click", function(event) {
            event.preventDefault();
            if (CekValidasi() == true) {
                if (cekdatadb() == true) {
                    $.confirm({
                        type: 'red',
                        title: 'Info..',
                        content: 'Apakah Anda Yakin Dengan Data Yang Diinput ?',
                        buttons: {
                            formSubmit: {
                                text: 'Ok',
                                btnClass: 'btn-red',
                                action: function() {
                                    $.ajax({
                                            url: "<?php echo base_url('karyawan/save'); ?>",
                                            method: "POST",
                                            dataType: "json",
                                            async: true,
                                            data: {
                                                namakaryawan: $('#namakaryawan').val(),
                                                tgllahirkaryawan: $('#tgllahirkaryawan').val(),
                                                alamatkaryawan: $('#alamatkaryawan').val(),
                                                hubungan_keluarga: $('#hubungankeluarga1').val(),
                                                nama_anggota_keluarga: $('#namakeluarga1').val(),
                                                tanggal_lahir_anggota_keluarga: $('#tgllahirkeluarga1').val()
                                            },
                                            success: function(data) {
                                                if (data.error == false) {
                                                    $.alert({
                                                        type: 'green',
                                                        title: 'Sukses!',
                                                        content: data.message,
                                                    });
                                                    Bersihkanlayarbaru();
                                                } else {
                                                    $.alert({
                                                        type: 'red',
                                                        title: 'Gagal!',
                                                        content: data.message,
                                                    });
                                                }
                                            }
                                        },
                                        false);
                                }
                            },
                            cancel: function() {
                                //close
                            },
                        },
                    });
                } else {
                    $.ajax({
                            url: "<?php echo base_url('karyawan/save'); ?>",
                            method: "POST",
                            dataType: "json",
                            async: true,
                            data: {
                                namakaryawan: $('#namakaryawan').val(),
                                tgllahirkaryawan: $('#tgllahirkaryawan').val(),
                                alamatkaryawan: $('#alamatkaryawan').val(),
                                hubungan_keluarga: $('#hubungankeluarga1').val(),
                                nama_anggota_keluarga: $('#namakeluarga1').val(),
                                tanggal_lahir_anggota_keluarga: $('#tgllahirkeluarga1').val()
                            },
                            success: function(data) {
                                if (data.error == false) {
                                    $.alert({
                                        type: 'green',
                                        title: 'Sukses!',
                                        content: data.message,
                                    });
                                    Bersihkanlayarbaru();
                                } else {
                                    $.alert({
                                        type: 'red',
                                        title: 'Gagal!',
                                        content: data.message,
                                    });
                                }
                            }
                        },
                        false);
                }
            }
        });

        $(document).on('click', ".searchcaridata", function() {
            var result = $(this).attr("data-id");
            datadetail(result);
        });

        function datadetail(kodekaryawan) {
            $.ajax({
                url: "<?php echo base_url('karyawan/datadetail'); ?>",
                method: "POST",
                dataType: "json",
                async: true,
                data: {
                    kodekaryawan: kodekaryawan
                },
                success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#kodekaryawan').val(data[i].m_employee_id);
                        $('#namakaryawan').val(data[i].nama_karyawan);
                        $('#tgllahirkaryawan').val(data[i].tanggal_lahir);
                        $('#alamatkaryawan').val(data[i].alamat);
                        $('#hubungankeluarga1').val(data[i].hubungan_keluarga);
                        $('#namakeluarga1').val(data[i].nama_anggota_keluarga);
                        $('#tgllahirkeluarga1').val(data[i].tanggal_lahir_anggota_keluarga);
                        document.getElementById('update').disabled = false;
                        document.getElementById('remove').disabled = false;
                        document.getElementById('add').disabled = true;
                    }
                }
            }, false);
        }

        document.getElementById("update").addEventListener("click", function(event) {
            event.preventDefault();
            $.confirm({
                type: 'blue',
                title: 'Info..',
                content: 'Apakah Anda Yakin ?',
                buttons: {
                    formSubmit: {
                        text: 'Ok',
                        btnClass: 'btn-blue',
                        action: function() {
                            $.ajax({
                                url: "<?php echo base_url('karyawan/update'); ?>",
                                method: "POST",
                                dataType: "json",
                                async: true,
                                data: {
                                    kodekaryawan: $('#kodekaryawan').val(),
                                    namakaryawan: $('#namakaryawan').val(),
                                    tgllahirkaryawan: $('#tgllahirkaryawan').val(),
                                    alamatkaryawan: $('#alamatkaryawan').val(),
                                    hubungan_keluarga: $('#hubungankeluarga1').val(),
                                    nama_anggota_keluarga: $('#namakeluarga1').val(),
                                    tanggal_lahir_anggota_keluarga: $('#tgllahirkeluarga1').val()
                                },
                                success: function(data) {
                                    if (data.error == false) {
                                        $.alert({
                                            title: 'Info..',
                                            content: data.message,
                                            buttons: {
                                                formSubmit: {
                                                    text: 'OK',
                                                    btnClass: 'btn-blue',
                                                    keys: ['enter', 'shift'],
                                                    action: function() {
                                                        BersihkanLayar();
                                                    }
                                                }
                                            }
                                        });
                                    } else {
                                        $.alert({
                                            title: 'Info..',
                                            content: data.message,
                                            buttons: {
                                                formSubmit: {
                                                    text: 'OK',
                                                    btnClass: 'btn-red',
                                                    keys: ['enter', 'shift'],
                                                    action: function() {
                                                        BersihkanLayar();
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        //close
                    },
                },
            });
        }, false);

        document.getElementById("remove").addEventListener("click", function(event) {
            event.preventDefault();
            $.confirm({
                type: 'red',
                title: 'Info..',
                content: 'Apakah Anda Yakin ?',
                buttons: {
                    formSubmit: {
                        text: 'Ok',
                        btnClass: 'btn-red',
                        action: function() {
                            $.ajax({
                                url: "<?php echo base_url('karyawan/delete'); ?>",
                                method: "POST",
                                dataType: "json",
                                async: true,
                                data: {
                                    kodekaryawan: $('#kodekaryawan').val()
                                },
                                success: function(data) {
                                    if (data.error == false) {
                                        $.alert({
                                            title: 'Info..',
                                            content: data.message,
                                            buttons: {
                                                formSubmit: {
                                                    text: 'OK',
                                                    btnClass: 'btn-red',
                                                    keys: ['enter', 'shift'],
                                                    action: function() {
                                                        BersihkanLayar();
                                                    }
                                                }
                                            }
                                        });
                                    } else {
                                        $.alert({
                                            title: 'Info..',
                                            content: data.message,
                                            buttons: {
                                                formSubmit: {
                                                    text: 'OK',
                                                    btnClass: 'btn-red',
                                                    keys: ['enter', 'shift'],
                                                    action: function() {
                                                        BersihkanLayar();
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        //close
                    },
                },
            });
        }, false);

        document.getElementById("new").addEventListener("click", function(event) {
            event.preventDefault();
            BersihkanLayar()
        }, false);
    });
</script>