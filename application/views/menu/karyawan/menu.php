<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">
                DATA KARYAWAN
            </h6>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group row">
                        <label class="col-md-3" for="namakaryawan">Nama Karyawan</label>
                        <div class="col-md-9">
                            <input class="form-control" type="hidden" name="kodekaryawan" id="kodekaryawan" maxlength="50" placeholder="Kode Karyawan" required>
                            <input class="form-control" type="text" name="namakaryawan" id="namakaryawan" maxlength="50" placeholder="Nama Karyawan" required>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group row">
                        <label class="col-md-3" for="tgllahirkaryawan">Tanggal Lahir</label>
                        <div class="col-md-9">
                            <input class="form-control" type="date" name="tgllahirkaryawan" id="tgllahirkaryawan" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group row">
                        <label class="col-md-3" for="alamatkaryawan">Alamat</label>
                        <div class="col-md-9">
                            <textarea name="alamatkaryawan" id="alamatkaryawan" class="form-control" placeholder="Alamat"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <br>

            <div class="card-header py-3">
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <h6 class="m-0 font-weight-bold text-primary">
                            DATA KELUARGA KARYAWAN
                        </h6>
                    </div>
                    <div class="col-lg-1 col-md-1">
                        <h6 class="text-right">
                            <button type="button" class="btn btn-block btn_remove" id="removedatakeluarga">
                                <i class="fa fa-minus" style="color: black; font-weight: bold;"></i>
                            </button>
                        </h6>
                    </div>
                    <div class="col-lg-1 col-md-1">
                        <h6 class="text-right">
                            <button type="button" class="btn btn-block ramovedatakeluarga" id="adddatakeluarga">
                                <i class="fa fa-plus" style="color: black; font-weight: bold;"></i>
                            </button>
                        </h6>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div id="datakeluarga1">
                    <div class="row after-add-more">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label class="col-md-3" for="hubungankeluarga1">Hubungan Keluarga</label>
                                <div class="col-md-9">
                                    <select name="hubungankeluarga1" class="form-control" id="hubungankeluarga1">
                                        <option value="-">-- Pilih Hubungan Keluarga --</option>
                                        <option value="istri">Istri</option>
                                        <option value="anak-1">Anak 1</option>
                                        <option value="anak-2">Anak 2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label class="col-md-3" for="namakeluarga1">Nama</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="namakeluarga1" id="namakeluarga1" placeholder="Nama Keluarga" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label class="col-md-3" for="tgllahirkeluarga1">Tanggal Lahir</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="date" name="tgllahirkeluarga1" id="tgllahirkeluarga1" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>

                <div id="datakeluargamore" style="display: none;">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label class="col-md-3" for="hubungankeluarga1">Hubungan Keluarga</label>
                                <div class="col-md-9">
                                    <select name="hubungankeluarga1" class="form-control" id="hubungankeluarga1">
                                        <option value="-">-- Pilih Hubungan Keluarga --</option>
                                        <option value="istri">Istri</option>
                                        <option value="anak-1">Anak 1</option>
                                        <option value="anak-2">Anak 2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label class="col-md-3" for="namakeluarga1">Nama</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="namakeluarga1" id="namakeluarga1" placeholder="Nama Keluarga" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row">
                                <label class="col-md-3" for="tgllahirkeluarga1">Tanggal Lahir</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="date" name="tgllahirkeluarga1" id="tgllahirkeluarga1" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-3 col-lg-2 mt-auto mb-2">
                        <button type="button" class="btn btn-success btn-block" id="new">
                            <i class="fa fa-sync">&nbsp; New</i>
                        </button>
                    </div>
                    <div class="col-md-3 col-lg-2 mt-auto mb-2">
                        <button type="button" class="btn btn-success btn-block" id="add">
                            <i class="fa fa-save">&nbsp; Save</i>
                        </button>
                    </div>
                    <div class="col-md-3 col-lg-2 mt-auto mb-2">
                        <button type="button" class="btn btn-info btn-block" id="update">
                            <i class="fa fa-edit">&nbsp; Update</i>
                        </button>
                    </div>
                    <div class="col-md-3 col-lg-2 mt-auto mb-2">
                        <button type="button" class="btn btn-danger btn-block" id="remove">
                            <i class="fa fa-trash-alt">&nbsp; Remove</i>
                        </button>
                    </div>
                    <div class="col-md-3 col-lg-2 mt-auto mb-2">
                        <input type="hidden" class="form-control" id="idkaryawan" width="50" readonly="">
                    </div>
                </div>

                <hr>

                <div class="col-md-12">
                    <span style="color: red; font-size: 10; font-weight: normal">Suggest : Untuk menghapus data, pilih data terlebih dahulu.</span>
                </div>

                <hr>

                <div class="table-responsive">
                    <div id="datakaryawan_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12 col-md-6"> Table Data Karyawan</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered dataTable no-footer" id="datakaryawan" width="100%" cellspacing="0" role="grid" aria-describedby="datakaryawan_info" style="width: 100%;">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 49px;">Action</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 38px;">Kode</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 44px;">Nama</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 44px;">Tanggal Lahir</th>
                                            <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 38px;">Alamat</th>
                                        </tr>
                                    </thead>
                                    <tbody id="detaildatakaryawan">
                                        <tr class="odd">
                                            <td valign="top" colspan="9" class="dataTables_empty">No data available in table</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="datakaryawan_processing" class="dataTables_processing card" style="display: none;">Processing...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>