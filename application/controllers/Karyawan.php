<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Karyawan_model');
        $this->load->model('caridata_model');
    }

    function datadetail()
    {
        $data = $this->Karyawan_model->datadetail($this->input->post('kodekaryawan'));
        echo json_encode($data);
    }

    function cekdata()
    {
        $data = $this->Karyawan_model->cekdata($this->input->post('kode'));
        echo json_encode($data);
    }

    function caridata()
    {
        $fetch_data = $this->caridata_model->make_datatables($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value'));
        $data = array();

        foreach ($fetch_data as $row) {
            $sub_array = array();
            $i = 1;
            $count = count($this->input->post('field'));
            foreach ($this->input->post('field') as $key => $value) {
                if ($i <= $count) {
                    if ($i == 1) {
                        $msearch = $row->$value;
                        $sub_array[] = '<button class="btn btn-primary searchcaridata" data-id="' . $msearch . '" data-dismiss="modal">Pilih <i class="fa fa-hand-point-right"></i></button> ';
                        $sub_array[] = $row->$value;
                    } else {
                        if ($i == $count) {
                            $sub_array[] = $row->$value;
                        } else {
                            $sub_array[] = $row->$value;
                        }
                    }
                }
                $i++;
            }
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                =>     intval($_POST["draw"]),
            "recordsTotal"        =>     $this->caridata_model->get_all_data($this->input->post('nmtb')),
            "recordsFiltered"     =>     $this->caridata_model->get_filtered_data($this->input->post('field'), $this->input->post('nmtb'), $this->input->post('sort'), $this->input->post('where'), $this->input->post('value')),
            "data"                =>     $data
        );
        echo json_encode($output);
    }

    function save()
    {
        $errorvalidasi = FALSE;
        // $kodekaryawan = $this->input->post('kodekaryawan');
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE);

        $ambilnomor = substr(date("Y"), 2, 2) . date("m");
        $get["KRY"] = $this->Karyawan_model->GetMaxNomor($ambilnomor);
        if (!$get["KRY"]->m_employee_id) {
            $nomor = $ambilnomor . "0001";
        } else {
            $lastNomor = $get['KRY']->m_employee_id;
            $lastNoUrut = substr($lastNomor, 4, 8);

            // nomor urut ditambah 1
            $nextNoUrut = $lastNoUrut + 1;
            $nomor = $ambilnomor . sprintf('%04s', $nextNoUrut);
        }

        if ($errorvalidasi == FALSE) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);
            $this->Karyawan_model->deletedata($nomor);
            $data = array(
                'm_employee_id' => $nomor,
                'nama_karyawan' => $this->input->post('namakaryawan'),
                'tanggal_lahir' => $this->input->post('tgllahirkaryawan'),
                'alamat' => $this->input->post('alamatkaryawan'),
                'hubungan_keluarga' => $this->input->post('hubungan_keluarga'),
                'nama_anggota_keluarga' => $this->input->post('nama_anggota_keluarga'),
                'tanggal_lahir_anggota_keluarga' => $this->input->post('tanggal_lahir_anggota_keluarga'),
                'valid_from' => date("Y-m-d"),
                'valid_to' => date("Y-m-d"),
                'create_by' => $nomor,
                'create_date' => date("Y-m-d H:i:s"),
                'update_by' => $nomor,
                'update_date' => date("Y-m-d H:i:s")
            );
            $this->Karyawan_model->save($data);
            // print_r($data);
            // die();

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'error' => true,
                    'message' => "Data gagal disimpan, Data sudah terdaftar"
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'error' => false,
                    'message' => "Data berhasil disimpan"
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
            return FALSE;
        }
    }

    public function Update()
    {
        $kodekaryawan = $this->input->post('kodekaryawan');

        $data = array(
            'm_employee_id' => $kodekaryawan,
            'nama_karyawan' => $this->input->post('namakaryawan'),
            'tanggal_lahir' => $this->input->post('tgllahirkaryawan'),
            'alamat' => $this->input->post('alamatkaryawan'),
            'hubungan_keluarga' => $this->input->post('hubungan_keluarga'),
            'nama_anggota_keluarga' => $this->input->post('nama_anggota_keluarga'),
            'tanggal_lahir_anggota_keluarga' => $this->input->post('tanggal_lahir_anggota_keluarga'),
            'valid_from' => date("Y-m-d"),
            'valid_to' => date("Y-m-d"),
            'create_by' => $kodekaryawan,
            'create_date' => date("Y-m-d H:i:s"),
            'update_by' => $kodekaryawan,
            'update_date' => date("Y-m-d H:i:s")
        );
        $result =  $this->Karyawan_model->Update($data, $kodekaryawan);

        if ($result == 1) {
            $resultjson = array(
                'error' => false,
                'message' => "Data berhasil diubah"
            );
        } else {
            $resultjson = array(
                'error' => false,
                'message' => "Data gagal diubah"
            );
        }
        echo json_encode($resultjson);
    }

    function delete()
    {
        $errorvalidasi = FALSE;
        $kodekaryawan = $this->input->post('kodekaryawan');

        if ($errorvalidasi == FALSE) {
            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            $this->Karyawan_model->delete($kodekaryawan);

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $resultjson = array(
                    'error' => true,
                    'message' => "Data gagal dihapus, Data tidak ada"
                );
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                $resultjson = array(
                    'error' => false,
                    'message' => "Data berhasil dihapus"
                );
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            echo json_encode($resultjson);
            return FALSE;
        }
    }
}
