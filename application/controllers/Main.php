<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('session');
        $this->load->library('Template');
    }

    //-- INDEX -- keluarga_karyawan
    public function index()
    {
        $data['title'] = "Karyawan";
        $data['script'] = "menu/karyawan/js";
        $this->template->view('menu/karyawan/menu', $data);
    }
    // -- END INDEX --
}
