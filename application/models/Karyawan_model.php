<?php defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
    function GetMaxNomor($nomor = "")
    {
        $this->db->select_max('m_employee_id');
        $this->db->where("left(m_employee_id,4)", $nomor);
        return $this->db->get('m_employee')->row();
    }

    function cekdata($kodekaryawan)
    {
        $this->db->where('m_employee_id', $kodekaryawan);
        return $this->db->get('m_employee')->result();
    }

    function datadetail($kodekaryawan)
    {
        $this->db->where('m_employee_id', $kodekaryawan);
        return $this->db->get('m_employee')->result();
    }

    function save($data)
    {
        $this->db->insert('m_employee', $data);
    }

    function Update($data = "", $kodekaryawan = "")
    {
        $this->db->where('m_employee_id', $kodekaryawan);
        return $this->db->update('m_employee', $data);
    }

    function delete($kodekaryawan = "")
    {
        $this->db->where('m_employee_id', $kodekaryawan);
        $this->db->delete('m_employee');
    }

    function deletedata($kodekaryawan)
    {
        $this->db->where('m_employee_id', $kodekaryawan);
        $this->db->delete('m_employee');
    }
}
